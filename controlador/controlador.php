<?php
/* AUTOR:
*  FECHA DE CREACIÓN:
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN:
*  ANOTACIONES: intermediario entre el Modelo y la Vista, gestionando el flujo de información entre
*  ellos y las transformaciones para adaptar los datos a las necesidades de cada uno.
*/
/*REQUERIMIENTOS DE ARCHIVOS*/

	/**
	 * Clase controlador
	 */
	class Controlador{

		/*VARIABLES Y CONSTANTES*/
		
		/**
		 * [pagina encargada de cargar el archivo plantilla.php]
		 */
		public function pagina(){
			include("vista/plantilla.php");
		}

		/**
		 * [enlacesPaginasControlador encargada de recoger el valor del accion en la barra de
		 * direcciones.]
		 */
		public function enlacesPaginasControlador(){
			isset($_GET['accion']) ? $enlaces = $_GET['accion'] : $enlaces = 'index=accion=inicio';
			$respuesta = Paginas::enlacesPaginasModelo($enlaces);
			include($respuesta);	
		}

		/**
		 * [listarUsuariosControlador obtiene los usuarios registrados en la base de datos]
		 * @return [object] [lista de usuarios obtenido de la base de datos]
		 */
		public function listartUsuariosControlador(){
			$respuesta = Modelo::listarUsuariosModelo('usuario');
			return $respuesta;
			// var_dump(array_key_exists('COD_ERR', $respuesta));
			// array_key_exists('CO_ERR', $respuesta);
			
		}


	}


?>