<div>
	<p class="display-3 text-center text-primary">La manera más fácil de comenzar a programar un proyecto web</p>
</div>
<div class="m-5" id="">
	<p>¿Cuantas veces has comenzado un proyecto web y pierdes mucho tiempo en establecer las bases del funcionamiento de dicho proyecto? Y lo peor de todo, que terminaas por crear un mounstruo de código, que al paso de unas semanas terminas sin entender lo que hiciste... que ni siquiera está documentado.</p>
	<p>Pues déjame decirte que traigo la solución a tu problema. Este proyecto es muy fácil de entender para que comiences a utilizarlo.</p>
	<p>Primero que nada, esta plantilla trabaja:
		<ul>
			<li>Para versiones de php 5 y 7, en adelante.</li>
			<li>Integra JavaScript en su version 3.4.1.</li>
			<li>Bootstrap como framework para diseño en su versión 4.3.1.</li>
			<li>Compatible con cualquier base de datos, siempre y cuando tengas el PDO configurado en tu servidor.</li>
			<li>Como arquitectura de trabajo, utilizamos el MVC</li>
		</ul>
	</p>
	<h3>Beneficios</h3>
	<ul>
		<li>Si eres nuevo en desarrollo web, aprenderás los fundamentos del MVC y una forma de integrar los tan populares lenguajes de programación como PHP, Javascript, HTML, y SQL.</li>
	</ul>
	<h3>¿Qué necesito saber?</h3>
	<ul>
		<li>Conocimientos básicos sobre la sintaxis de:</li>
		<ul>
			<li>PHP</li>
			<li>Javascript</li>
			<li>HTML</li>
			<li>SQL</li>
		</ul>
		<li>¡Debes saber programar! sin importar en qué lenguaje o incluso, si nunca has codificado pero conoces los fundamentos de la programación estructurada y orientada a objetos</li>
	</ul>

	<h3>Un poco sobre mi...</h3>
</div>
	
	<?php
		// require_once "modulos/extras/modal_error.html";
		// require_once "modulos/extras/modal_exito.html";
		// require_once "modulos/extras/modal_info.html";
	?>