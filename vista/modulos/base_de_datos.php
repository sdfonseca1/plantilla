<p class="display-4 text-center text-primary">Ejemplo de consultas a la base de datos</p>

<span id="mensaje_alerta" class="oculto"></span>
<div class="container row oculto text-center offset-md-1" id="tabla_usuarios">
	<table class="table table-responsive">
		<thead class="thead-dark">
			<tr>
				<th>#</th>
				<th>Usuario</th>
				<th>Contraseña</th>
			</tr>
		</thead>
		<tbody id="contenido_usuarios"></tbody>
	</table>
</div>