<?php
/* AUTOR:
*  FECHA DE CREACIÓN:
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN:
*  ANOTACIONES: archivo encargado de comunicar las peticiones AJAX con el controlador.
*/

/*VARIABLES Y CONSTANTES*/

/*REQUERIMIENTOS DE ARCHIVOS*/
require_once "../controlador/controlador.php";
require_once "../modelo/modelo.php";

	//@var string 	nombre de la función que se necesita para ejecutar en el controlador.
	$funcion = $_POST["funcion"];

	switch ($funcion) {
		case 'listarUsuarios':
			$respuesta = Controlador::listartUsuariosControlador();
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
	}




?>
