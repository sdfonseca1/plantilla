<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Inicio</title>

	<!-- ARCHIVOS BOOTSTRAP -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<!-- ESTILOS PERONALIZADOS -->
	<link rel="stylesheet" type="text/css" href="css/estilos.css">
	<!-- ARCHIVOS JAVASCRIPT -->
	<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</head>
<body>
	<!-- Barra de navegación -->
	<?php
		require_once "modulos/barra_navegacion.html";
	?>
	<!-- Enlace de las páginas -->
	<div class="container my-4 p-4">
		<?php
            if ($_SERVER['REQUEST_URI'] == "/plantilla/"){
                header('location:index.php?accion=inicio');
            }
            $mvc = new Controlador();
            $mvc->enlacesPaginasControlador();
        ?>
    </div>

	<!-- ----------------------------------------------------------------------------------------------- -->
	<?php
		require_once "vista/modulos/extras/modal_error.html";
		require_once "vista/modulos/extras/modal_exito.html";
		require_once "vista/modulos/extras/modal_info.html";
	?>
	
	<!-- Pie de página -->

	<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	
</body>
</html>