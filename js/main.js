$(document).ready( function (){
	listarUsuarios();
});

/**
 * [listarUsuarios muestra los usuarios registrados en la base de datos]
 * @return {[type]} [description]
 */
function listarUsuarios(){
	var datos = {
		funcion: 'listarUsuarios'
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR")) {
				mostrarModalAlerta('error', respuesta['COD_ERR']+' '+respuesta['ERR_DRI']+' '+respuesta['ERR_MSG']);
			}else{
				if (respuesta == 0){
					$('#mensaje_alerta').css('display','block').addClass('alert alert-warning text-center').removeClass('oculto')
					.append('No existen usuarios registrados.');
				}else{
					$('#tabla_usuarios').css('display', 'block');
					let i = 1;
					respuesta.forEach(function (e){
						$('#contenido_usuarios').append('<tr><td>'+i+'</td><td>'+e[0]+'</td><td>'+e[1]+'</td></tr>')
					});
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', 'Error al cargar los datos de los usuarios: '+errorThrown);
		}
		
	})
}

/**
 * [mostrarModalAlerta mostrar modal de acción exitosa]
 * @param  {[text} tipo  [tipo de mensaje: error, info, exito]
 * @param  {[text} texto [mensaje que será desplegado]
 */
function mostrarModalAlerta(tipo, texto){
	$('#texto_mensaje_'+tipo).append(texto);
	$("#alerta_"+tipo).modal("show");
}

/**
 * [limpiarContenido vacia el contenido de un objeto del DOM por id]
 * @param  {[text]} id [id del objeto]
 */
function limpiarContenido(id){
	$('#'+id).empty();
}

/**
 * [cerrarModal cierra un modal, identificado por id]
 * @param  {[text]} id [id del modal a cerrar]
 */
function cerrarModal(id){
	$('#'+id).modal('hide');
}

/**
 * [mostrarModal muestra un modal, identificado por id]
 * @param  {[ttext]} id [id del modal a mostrar]
 */
function mostrarModal(id){
	$('#'+id).modal('show');
}

/**
 * [deshabilitarElementoID deshabilita un elemento por id]
 * @param  {[text]} id [id del elemento a deshabilitat]
 */
function deshabilitarElementoID(id){
	$("#"+id).attr('disabled', '');
}

/**
 * [habilitarElementoID habilita un elemento por id]
 * @param  {[text]} id [id del elemento a habilitar]
 */
function habilitarElementoID(id){
	$("#"+id).removeAttr('disabled', '');
}