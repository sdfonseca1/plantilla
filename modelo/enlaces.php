<?php
/** AUTOR:
 *  FECHA DE CREACIÓN:
 *  FECHA DE ÚLTIMA MODIFICACIÓN:
 *  DESCRIPCIÓN: Encargado de recoger los valores en la barra de dirección para rederigir entre
 *  las distintas vistas.
 *  ANOTACIONES: se recomienda usar la extensión DocBlocker para activar las funciones rápidas de
 *  comentado de variable y funciones.
 *
 */


/*VARIABLES Y CONSTANTES*/
	
	/**
	 * [$mi_variable texto de para qué se usaría una variable]
	 * @var string
	 */
	$mi_variable = "valor";
	
	class Paginas{

		/**
		 * [enlacesPaginasModelo encarga de obtener la ruta del recurso a cargar]
		 * @param  [text] $enlace [enlace de la barra de dirreciones]
		 * @return [text] $modulo  [ruta del archivo que se desplegará]
		 */
		public function enlacesPaginasModelo($enlace){
			if($enlace == 'inicio')
			   	$modulo = "vista/modulos/inicio.php";
			else if ($enlace == 'basededatos')
				$modulo = 'vista/modulos/base_de_datos.php';
			return $modulo;
		}
	}
?>	