<?php
/* AUTOR:
*  FECHA DE CREACIÓN:
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN:
*  ANOTACIONES: el archivo de conexión a la base de datos.
*/
	class Conexion{

		/*VARIABLES Y CONSTANTES*/

		//@var string 	nombre de usuario para la base de datos.
		var $usuario = "root";
		//@var string 	contraseña para la base de datos.
		var $contrasenha = "SDF10ico-@";
		//@var string 	nombre de la base de datos.
		var $base_datos = "plantilla";
		//@var string 	nombre del servidor de la base de datos.
		var $servidor = "localhost";

		/**
		 * [conectar realiza la conexión a una base de datos específica]
		 * @return [object] [regresa el objeto PDO de la conexión o el de error]
		 */
		public function conectar(){
			try{
				//$link = new PDO("mysql:host=".$servidor.";dbname=".$base_datos,$usuario,$contrasenha);
				$link = new PDO("mysql:host=localhost;dbname=plantilla", "root", "SDF10ico-@");
				return $link;
			}catch(PDOException $e){
			 	echo "Error: " . $e->getMessage();
			}
		}
	}

?>